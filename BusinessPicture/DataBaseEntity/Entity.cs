﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessPicture.DataBaseEntity
{
    class Entity
    {
        private int id;

        public Entity(int id)
        {
            this.id = id;
        }

        public int Id { get => id; set => id = value; }
    }
}
