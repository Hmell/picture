﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessPicture.Helpers
{
    class Client
    {
        private int id;
        private string first_name;
        private string last_name;
        private string father_name;
        private string phone;
        private string email;
        private string description;

        public Client() { }

        public Client(int id, string first_name, string last_name, string father_name, string phone, string email, string description)
        {
            this.id = id;
            this.first_name = first_name;
            this.last_name = last_name;
            this.father_name = father_name;
            this.phone = phone;
            this.email = email;
            this.description = description;
        }

        public Client(object[] clientObject)
        {
            this.id = clientObject[0] != null ? (int)clientObject[0] : 0;
            this.first_name = clientObject[1] != null ? (string)clientObject[1] : null;
            this.last_name = clientObject[2] != null ? (string)clientObject[2] : null;
            this.father_name = clientObject[3] != null ? (string)clientObject[3] : null;
            this.phone = clientObject[4] != null ? (string)clientObject[4] : null;
            this.email = clientObject[5] != null ? (string)clientObject[5] : null;
            this.description = clientObject[6] != null ? (string) clientObject[6] : "";
        }

        public int DownIndex { get { return Id - 1; } }

        public string FullName { get { return string.Format("{0} {1} {2}", last_name, first_name, father_name); } }

        public string ShortFullName { 
            get {     
                if (last_name.Length > 0 && first_name.Length > 0 && father_name.Length > 0) 
                    return string.Format("{0} {1}. {2}.", last_name, first_name.Length == 0 ? "" : first_name.Substring(0, 1), first_name.Length == 0 ? "" : father_name.Substring(0, 1));
                else return FullName; 
        }   }

        public int Id { get => id; set => id = value; }
        public string First_name { get => first_name; set => first_name = value; }
        public string Last_name { get => last_name; set => last_name = value; }
        public string Father_name { get => father_name; set => father_name = value; }
        public string Phone { get => phone; set => phone = value; }
        public string Email { get => email; set => email = value; }
        public string Description { get => description; set => description = value; }
    }
}
