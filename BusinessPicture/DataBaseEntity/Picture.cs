﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessPicture.Helpers
{
   class Picture
    {
        private int id;
        private PictureSize size;
        private PictureStyle style;
        private int peopleOnPicture;
        private string vanileImage;
        private string customImage;

        public Picture(int id, PictureSize size, PictureStyle style, int peopleOnPicture)
        {
            this.id = id;
            this.size = size;
            this.style = style;
            this.peopleOnPicture = peopleOnPicture;
        }

        public Picture(object[] list)
        {
            this.id = !list[0].GetType().Equals(DBNull.Value) ? (int)list[0] : 0;
            this.style = new PictureStyle((int)list[1],(string)list[2],(string)list[3]); 
            this.size = new PictureSize((int)list[4], (double)list[5], (double)list[6]);
            this.peopleOnPicture = !list[7].GetType().Equals(DBNull.Value) ? (int)list[7] : 0; 
            this.VanileImage = !list[8].GetType().Equals(DBNull.Value) ? (string)list[8] : "";
            bool t = !list[9].GetType().Equals(DBNull.Value);
            this.CustomImage = t ? (string)list[9] : ""; 
        }

        public int Id { get => id; set => id = value; }
        public PictureSize Size { get => size; set => size = value; }
        public PictureStyle Style { get => style; set => style = value; }
        public int PeopleOnPicture { get => peopleOnPicture; set => peopleOnPicture = value; }
        public string VanileImage { get => vanileImage; set => vanileImage = value; }
        public string CustomImage { get => customImage; set => customImage = value; }
    }

    class PictureSize
    {
        private int id;
        private double height;
        private double width;

        public PictureSize(int id, double height, double width)
        {
            this.id = id;
            this.height = height;
            this.width = width;
        }

        public PictureSize(object[] ordersObjects)
        {
            this.Id = ordersObjects[0] != null ? (int)ordersObjects[0] : 0;
            this.Height = ordersObjects[1] != null ? (double)ordersObjects[1] : 0;
            this.Width = ordersObjects[2] != null ? (double)ordersObjects[2] : 0;
        }

        public string size { get { return height != 0 ? height + "x" + width : "Индивидуальный"; } }
        public int ComboIndex { get { return Id - 1; } }

        public int Id { get => id; set => id = value; }
        public double Height { get => height; set => height = value; }
        public double Width { get => width; set => width = value; }
    }

    class PictureStyle
    {
        private int id;
        private string name;
        private string description;

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Description { get => description; set => description = value; }

        public int ComboIndex { get => Id-1; }

        public PictureStyle(int id, string name, string description)
        {
            this.id = id;
            this.name = name;
            Description = description;
        }

        public PictureStyle(object[] ordersObjects)
        {
            this.Id = ordersObjects[0] != null ? (int)ordersObjects[0] : 0;
            this.Name = ordersObjects[1] != null ? (string)ordersObjects[1] : "";
            this.Description = ordersObjects[2] != null ? (string)ordersObjects[2] : "";
        }
    }
}
