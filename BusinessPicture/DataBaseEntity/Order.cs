﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessPicture.Helpers
{
    class Order
    {
        private int id;
        private Client client;
        private Picture picture;
        private int quantity;
        private Decimal price;
        private string notation;
        private bool done;

        private bool needPackage;
        private bool needDelivery;
        private int discount;
        private DateTime date;

        public Order() { }

        public Order(int id, Client client, Picture picture, int quantity, Decimal price, string notation, bool done)
        {
            this.Id = id;
            this.Client = client;
            this.Picture = picture;
            this.Quantity = quantity;
            this.Price = price;
            this.Notation = notation;
            this.Done = done;
        }

        public Order(int id, Client client, Picture picture, int quantity, decimal price, string notation, bool done, bool needPackage, bool needDelivery) : this(id, client, picture, quantity, price, notation, done)
        {
            this.needPackage = needPackage;
            this.needDelivery = needDelivery;
            Date = DateTime.Now;
        }

        public Order(object[] ordersObjects, bool pack, bool del)
        {
            DBManager db = new DBManager();

            this.Id = ordersObjects[0] != null ? (int)ordersObjects[0] : 0;
            bool f =  ordersObjects[1].ToString() == string.Empty;
            this.Client = f ? null : (Client) db.GetEntity(DBManager.Entity.E_CLIENT, (int) ordersObjects[1]);
            this.Picture = (Picture) db.GetEntity(DBManager.Entity.E_PICTURE, (int)ordersObjects[2]);
            this.Price = ordersObjects[3] != null ? (Decimal)ordersObjects[3] : 0;
            this.Notation = ordersObjects[4] != null ? (string)ordersObjects[4] : "";
            this.Done = (bool)ordersObjects[5];// ? true  : false;
            this.NeedPackage = pack;
            this.NeedDelivery = del; 
            this.Quantity = ordersObjects[6] != null ? (int)ordersObjects[6] : 0;
            this.Discount = ordersObjects[7] != null ? (int)ordersObjects[7] : 0;
            //DateTime time = new DateTime();
            this.Date = ordersObjects[8] != null ?  new DateTime(long.Parse((string)ordersObjects[8])) : new DateTime();
        }

        public void setAdditional(bool pack, bool delivery)
        {
            NeedDelivery = delivery;
            NeedPackage = pack;
        }

        public int Id { get => id; set => id = value; }
        public int Quantity { get => quantity; set => quantity = value; }
        public Decimal Price { get => price; set => price = value; }
        public string Notation { get => notation; set => notation = value; }
        public bool Done { get => done; set => done = value; }
        public Client Client { get => client; set => client = value; }
        public Picture Picture { get => picture; set => picture = value; }
        public bool NeedPackage { get => needPackage; set => needPackage = value; }
        public bool NeedDelivery { get => needDelivery; set => needDelivery = value; }
        public int Discount { get => discount; set => discount = value; }

        public string Status { get => Done ? "Выполнен" : "В процессе"; }
        public DateTime Date { get => date; set => date = value; }
    }

    public class Additional
    {
        int id;
        string caption;
        double price;

        public Additional(int id, string caption, double price)
        {
            this.Id = id;
            this.Caption = caption;
            this.Price = price;
        }

        public Additional(object[] ordersObjects)
        {
            this.Id = ordersObjects[0] != null ? (int)ordersObjects[0] : 0;
            this.Caption = ordersObjects[1] != null ? (string)ordersObjects[1] : "";
            this.Price = ordersObjects[2] != null ? (double)ordersObjects[2] : 0;
        }

        public int Id { get => id; set => id = value; }
        public string Caption { get => caption; set => caption = value; }
        public double Price { get => price; set => price = value; }
    }

}
