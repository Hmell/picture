﻿using BusinessPicture.Helpers;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace BusinessPicture.ViewModels
{
    class MainPageViewModel : ViewModelBase
    {

        public ObservableCollection<ClientViewModel> ClientsList { get; set; }
        public ObservableCollection<Order> OrdersList { get; set; }

        public MainPageViewModel()
        {

            DBManager db = new DBManager();

            ClientsList = new ObservableCollection<ClientViewModel>(db.GetEntityes(DBManager.Entity.E_CLIENT).Select(b => new ClientViewModel((Client)b)));
            OrdersList = new ObservableCollection<Order>(db.GetEntityes(DBManager.Entity.E_ORDER).Select(b => (Order) b));
        }

        private ICommand _onAddOrder;

        public ICommand OnAddOrder
        {
            get
            {
                if (_onAddOrder == null)
                {
                    _onAddOrder = new RelayCommand(obj =>
                    {
                        string uri = "Pages/OrderPage.xaml";
                        AppContext.fr.NavigationService.Navigate(new Uri(uri, UriKind.Relative));
                    });
                }
                return _onAddOrder;
            }
        }

        private ICommand _onAddClient;

        public ICommand OnAddClient
        {
            get
            {
                if (_onAddClient == null)
                {
                    _onAddClient = new RelayCommand(obj =>
                    {
                        string uri = "Pages/ClientPage.xaml";
                        AppContext.fr.NavigationService.Navigate(new Uri(uri, UriKind.Relative));
                    });
                }
                return _onAddClient;
            }
        }


    }
}
