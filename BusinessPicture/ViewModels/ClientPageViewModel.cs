﻿using BusinessPicture.Helpers;
using GalaSoft.MvvmLight.Command;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace BusinessPicture.ViewModels
{
    class ClientPageViewModel : ViewModelBase
    {
        public ObservableCollection<ClientViewModel> ClientsList { get; set; }
        public ObservableCollection<Order> OrdersList { get; set; }

        DBManager db = new DBManager();



        public ClientPageViewModel()
        {

            checkedWorkSpace = true;

            

            ClientsList = new ObservableCollection<ClientViewModel>(db.GetEntityes(DBManager.Entity.E_CLIENT).Select(b => new ClientViewModel((Client)b)));
            OrdersList = new ObservableCollection<Order>();
            SearchString = "";
        }


        private ClientViewModel selected;

        public ClientViewModel Selected 
        { 
            get { return selected; }
            set
            {
                selected = value;
                OrdersList.Clear();
                if (selected != null)
                {
                    foreach (Order item in db.GetOrderForClients(DBManager.Entity.E_ORDER, selected.id))
                    {
                        OrdersList.Add(item);
                    }
                }

                OnPropertyChanged("Selected");
                OnPropertyChanged("isOrderSelected");
            }
        }

        public Visibility isOrderSelected
        {
            get { return Selected == null ? Visibility.Visible : Visibility.Collapsed; }
        }


        private bool checkedWorkSpace;

        public bool CheckedWorkSpace
        {
            get { return checkedWorkSpace; }
            set
            {
                checkedWorkSpace = value;

                OnPropertyChanged("CheckedWorkSpace");
                if (checkedWorkSpace)
                    OnChangeWorkSpace();

            }
        }

        private void OnChangeWorkSpace()
        {


                Client client = ClientsList.First(p => p.client.Id == selected.client.Id).client;
                if (client.Equals(selected.client))
                {
                    DBManager.UpdateEntity(DBManager.Entity.E_CLIENT, client);
                }
            
        }


        public string SearchString { get; set; }

        private ICommand _onSearch;

        public ICommand OnSearch
        {
            get
            {
                if (_onSearch == null)
                {
                    _onSearch = new RelayCommand(obj =>
                    {
                        ObservableCollection<Client> list = new ObservableCollection<Client>(db.GetEntityes(DBManager.Entity.E_CLIENT).Select(b => (Client)b));
                        ClientsList.Clear();
                        Selected = null;

                        foreach (Client item in list)
                        {
                            if (SearchString.Trim().Equals("") || item.FullName.Contains(SearchString) || item.Phone.Contains(SearchString))
                                {
                                    ClientsList.Add(new ClientViewModel(item));
                                }
                            }
                        


                    });
                }
                return _onSearch;
            }
        }


        private ICommand _onAddCommand;

        public ICommand onAddCommand
        {
            get
            {
                if (_onAddCommand == null)
                {
                    _onAddCommand = new RelayCommand(obj =>
                    {
                        Client client = new Client(-1,"", "","","","","");
                        int eout =  DBManager.AddEntity(DBManager.Entity.E_CLIENT,client);

                        //MessageBox.Show(eout == -1 ? "Не удалось" : "Клиент добавлен");
                        if (eout != -1)
                        {
                            ClientViewModel c = new ClientViewModel(client);
                            c.id = eout;
                            ClientsList.Add(c);
                            Selected = c;
                            CheckedWorkSpace = false;
                        }

                    });
                }
                return _onAddCommand;
            }
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDelete
        {
            get
            {
                if (_onDeleteCommand == null)
                {
                    _onDeleteCommand = new RelayCommand(obj =>
                    {
                        
                        bool eout = DBManager.DeleteEntity(DBManager.Entity.E_CLIENT, selected.client.Id);

                        MessageBox.Show(!eout ? "Не удалось" : "Клиент удален");
                        if (eout)
                            ClientsList.Remove(selected);
                    });
                }
                return _onDeleteCommand;
            }

        }

    }
}
