﻿using BusinessPicture.Helpers;
using GalaSoft.MvvmLight.CommandWpf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace BusinessPicture.ViewModels
{
    class OrderPageViewModel : ViewModelBase
    {

        private readonly DBManager db = new DBManager();

        private OrderViewModel nullOrder;

        public ObservableCollection<OrderViewModel> OrdersList { get; set; }

        public ObservableCollection<Client> Clients { get; set; }
        public ObservableCollection<PictureSize> PicSize  { get; set; }
        public ObservableCollection<PictureStyle> PicStyle { get; set; }

        public ObservableCollection<string> DiscountComboList { get; set; }

        public OrderPageViewModel()
        {

            OrdersList = new ObservableCollection<OrderViewModel>(db.GetEntityes(DBManager.Entity.E_ORDER).Select(b => new OrderViewModel((Order)b)));
            Clients = new ObservableCollection<Client>(db.GetEntityes(DBManager.Entity.E_CLIENT).Select(b => (Client) b ));

            PicSize = new ObservableCollection<PictureSize>(db.GetEntityes(DBManager.Entity.E_PICSIZE).Select(b => (PictureSize) b ));
            //SelectedPicSize = PicSize[0];
            PicStyle = new ObservableCollection<PictureStyle>(db.GetEntityes(DBManager.Entity.E_PICSTYLE).Select(b => (PictureStyle) b ));


            DiscountComboList = new ObservableCollection<string> { "0%", "5%", "10%", "15%", "20%", "25%", "30%" };


            nullOrder = new OrderViewModel(new Order(-1, null, new Picture(DBManager.AddEntity(DBManager.Entity.E_PICTURE, new Picture(-1, PicSize[0], PicStyle[0], 1)), PicSize[0], PicStyle[0], 1), 1, 0M, "", false, false, false));
            SearchString = "";
        }

        public string VanileImage
        {
            get 
            {
                if (SelectedOrder != null && SelectedOrder.Picture != null)
                {
                    if (SelectedOrder.Picture.VanileImage == "")
                        return "/Images/placeholder.png";
                    else
                        return SelectedOrder.Picture.VanileImage;
                }
                return "/Images/placeholder.png";
            }
            set
            {
                SelectedOrder.Picture.VanileImage = value;

                OnPropertyChanged("VanileImage");
            }
        }

        public string CustomImage
        {
            get
            {
                if (SelectedOrder != null && SelectedOrder.Picture != null)
                {
                    if (SelectedOrder.Picture.CustomImage == "")
                        return "/Images/placeholder.png";
                    else
                        return SelectedOrder.Picture.CustomImage;
                }
                return "/Images/placeholder.png";
            }
            set
            {
                SelectedOrder.Picture.CustomImage = value;

                OnPropertyChanged("CustomImage");
            }
        }

        public string searchString;
        public string SearchString {
            get
            {
                if (!searchString.Equals("BusinessPicture.Helpers.Client"))
                    return searchString;
                else
                    return SelectedOrder.Client.FullName;
                    
            }
            set 
            {
                if(value != null)
                if(!value.Equals("BusinessPicture.Helpers.Client"))
                        searchString = value;

            }
        }

        
        private ICommand _onSearch;

        public ICommand OnSearch
        {
            get
            {
                if (_onSearch == null)
                {
                    _onSearch = new RelayCommand(obj =>
                    {
                        ObservableCollection<Client> list = new ObservableCollection<Client>(db.GetEntityes(DBManager.Entity.E_CLIENT).Select(b => (Client)b));
                        Clients.Clear();
                        //Selected = null;

                        foreach (Client item in list)
                        {
                            if (SearchString.Trim().Equals("") || item.FullName.Contains(SearchString) || item.Phone.Contains(SearchString))
                            {
                                Clients.Add(item);
                            }
                        }



                    });
                }
                return _onSearch;
            }
        }

        public decimal Price
        {
            get { return selectedOrder != null ? selectedOrder.Price : 0M; }
            set
            {
                if(selectedOrder != null)
                selectedOrder.Price = value;
                OnPropertyChanged("Price");
                OnPropertyChanged("SumWithDiscount");
            }
        }


        public string DiscountText
        {
            get { 
                if (selectedOrder != null)
                    return selectedOrder.DiscountText;
                else
                    return "0%";
            }
            set {
                if (value != null)
                {
                   selectedOrder.DiscountText = value;

                    OnPropertyChanged("DiscountText");
                    UpdateProperty();
                }
            }
        }

        

        public int POP
        {
            get
            {
                if (SelectedOrder == null)
                    return 1;
                return SelectedOrder.Picture.PeopleOnPicture;
            }
            set
            {
                SelectedOrder.Picture.PeopleOnPicture = value;
                OnPropertyChanged("POP");
                UpdateProperty();
            }
        }


        public int Quantity
        {
            get
            {
                if (SelectedOrder == null)
                    return 1;
                return SelectedOrder.quantity;
            }
            set
            {
                SelectedOrder.quantity = value;
                OnPropertyChanged("Quantity");
                UpdateProperty();
            }
        }

        public decimal SumWithDiscount
        {
            get
            {
                if (SelectedOrder != null)
                    if (SelectedOrder.Discount > 0)
                    {
                        decimal start_sum = SelectedOrder.TotalPrice;
                        decimal procent = SelectedOrder.Discount / 100M;
                        return start_sum - (start_sum * procent);
                    }
                    else return SelectedOrder.TotalPrice;
                else return 0M;
            }
        }

        private OrderViewModel selectedOrder;
        public OrderViewModel SelectedOrder
        {
            get { return selectedOrder; }
            set
            {
                selectedOrder = value;
                if (selectedOrder != null && selectedOrder.Picture != null)
                {
                    SelectedPicSize = selectedOrder.Picture.Size;
                    if (selectedOrder.IsDone)
                        bntVisibility = Visibility.Collapsed;
                    else
                        bntVisibility = Visibility.Visible;
                }
                OnPropertyChanged("SelectedOrder");
                UpdateProperty();
            }
        }

        public Visibility isOrderSelected
        {
            get { return selectedOrder == null ? Visibility.Visible : Visibility.Collapsed; }
        }


        public string selectedWidth;
        public string selectedHeight;
        private PictureSize selectedPicSize;

        public PictureSize SelectedPicSize
        {
            get { return selectedPicSize; }
            set
            {
                if (value != null)
                {
                    selectedPicSize = value;
                    selectedOrder.Picture.Size = value;
                    string[] arr = value.size.Split('x');
                    if (arr.Length > 1)
                    {
                        selectedWidth = arr[0];
                        selectedHeight = arr[1];
                    }
                    else
                    {
                        selectedWidth = "";
                        selectedHeight = "";
                    }

                    OnPropertyChanged("SelectedPicSize");
                    UpdateProperty();
                }
            }
        }

        private PictureStyle selectedPicStyle;

        public PictureStyle SelectedPicStyle
        {
            get { return selectedPicStyle; }
            set
            {
                if (value != null)
                {
                    selectedPicStyle = value;
                    selectedOrder.Picture.Style = value;
                    OnPropertyChanged("SelectedPicStyle");
                    UpdateProperty();
                }
            }
        }

        private string filterComboValue;
        public ComboBoxItem FilterComboValue
        {
            set
            {
                filterComboValue = (string)value.Content;
                ObservableCollection<OrderViewModel> oc = OrdersList;
                switch (filterComboValue)
                {
                    case "Без фильтрации":
                        {
                            OrdersList = new ObservableCollection<OrderViewModel>(OrdersList.OrderBy(item => item.Id));
                        }
                        break;
                    case "Выполненные":
                        oc = new ObservableCollection<OrderViewModel>(OrdersList.OrderBy(item => !item.IsDone));
                        OrdersList.Clear();
                        foreach (OrderViewModel item in oc) OrdersList.Add(item);
                        break;
                    case "В процессе":
                        oc = new ObservableCollection<OrderViewModel>(OrdersList.OrderBy(item => item.IsDone));
                        OrdersList.Clear();
                        foreach (OrderViewModel item in oc) OrdersList.Add(item);
                        break;
                }
            }
        }

        public bool NeedPackage
        {
            get {
                if (SelectedOrder == null)
                    return false;
                return SelectedOrder.NeedPackage; }
            set
            {
                SelectedOrder.NeedPackage = value;
                OnPropertyChanged("Done");
                UpdateProperty();
            }
        }

        public bool NeedDelivery
        {
            get {
                if (SelectedOrder == null)
                    return false;
                return SelectedOrder.NeedDelivery; 
            }
            set
            {
                SelectedOrder.NeedDelivery = value;
                OnPropertyChanged("Done");
                UpdateProperty();
            }
        }

        private Visibility _bntVisibility = Visibility.Visible;

        public Visibility bntVisibility
        {
            get { return _bntVisibility; }
            set
            {
                _bntVisibility = value;
                OnPropertyChanged("bntVisibility");
            }
        }

        private ICommand _onAddCommand;

        public ICommand onAddCommand
        {
            get
            {
                if (_onAddCommand == null)
                {
                    _onAddCommand = new RelayCommand(obj =>
                    {

                        Order newOrder = new Order(-1, null,new Picture(DBManager.AddEntity(DBManager.Entity.E_PICTURE,new Picture(-1,PicSize[0], PicStyle[0], 1)), PicSize[0],PicStyle[0],1),1,0M,"",false,false, false);
                        int eout = DBManager.AddEntity(DBManager.Entity.E_ORDER, newOrder);
                        newOrder.Id = eout;
                        OrdersList.Add(new OrderViewModel(newOrder));
                        SelectedOrder = OrdersList.Last();
                        UpdateProperty();
                    });
                }
                return _onAddCommand;
            }
        }

        private ICommand _onSaveChanges;

        public ICommand OnSaveChanges
        {
            get
            {
                if (_onSaveChanges == null)
                {
                    _onSaveChanges = new RelayCommand(obj =>
                    {
                        if (VanileImage != null)
                        {
                            string expansion = VanileImage.Substring(VanileImage.LastIndexOf('.'));
                            string newPath = Environment.CurrentDirectory + "/Images/Vanile." + SelectedOrder.Id + "." + SelectedOrder.Picture.Style.Name + "." + SelectedOrder.Client.Last_name + expansion;
                            File.Copy(VanileImage, newPath, true);
                        }
                        if (CustomImage != null)
                        {
                            string expansion2 = CustomImage.Substring(CustomImage.LastIndexOf('.'));
                            string newPath2 = Environment.CurrentDirectory + "/Images/Custom." + SelectedOrder.Id + "." + SelectedOrder.Picture.Style + "." + SelectedOrder.Client.FullName + expansion2;
                            File.Copy(CustomImage, newPath2, true);
                        }
                        DBManager.UpdateEntity(DBManager.Entity.E_PICTURE, selectedOrder.Picture);
                        DBManager.UpdateEntity(DBManager.Entity.E_ORDER, selectedOrder.order);
                    });
                }
                return _onSaveChanges;
            }
        }


        private ICommand _onDone;

        public ICommand OnDone
        {
            get
            {
                if (_onDone == null)
                {
                    _onDone = new RelayCommand(obj =>
                    {

                        MessageBoxButton button = MessageBoxButton.YesNo;
                        MessageBoxResult result = MessageBox.Show("Заказ выполнен?", "Подтверждение действия", button);
                        if(result == MessageBoxResult.Yes)
                        {
                            SelectedOrder.IsDone = true;


                            DBManager.UpdateEntity(DBManager.Entity.E_PICTURE, selectedOrder.Picture);
                            DBManager.UpdateEntity(DBManager.Entity.E_ORDER, selectedOrder.order);
                        }

                        

                    });
                }
                return _onDone;
            }
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDelete
        {
            get
            {
                if (_onDeleteCommand == null)
                {
                    _onDeleteCommand = new RelayCommand(obj =>
                    {
                        bool eout = DBManager.DeleteEntity(DBManager.Entity.E_ORDER, selectedOrder.Id);

                        if (eout)
                        {
                            OrdersList.Remove(SelectedOrder);
                            SelectedOrder = nullOrder;
                            
                        }
                    });
                }
                return _onDeleteCommand;
            }

        }


        

        private ICommand _OnSelectVanileImage;

        public ICommand OnSelectVanileImage
        {
            get
            {
                if (_OnSelectVanileImage == null)
                {
                    _OnSelectVanileImage = new RelayCommand(obj =>
                    {
                        var dialog = new OpenFileDialog() { Filter = "Image files (*.BMP, *.JPG, *.GIF, *.TIF, *.PNG, *.ICO, *.EMF, *.WMF)|*.bmp;*.jpg;*.gif; *.tif; *.png; *.ico; *.emf; *.wmf" };

                        if (dialog.ShowDialog() == true)
                        {

                            VanileImage = dialog.FileName;
                        }
                    });
                }
                return _OnSelectVanileImage;
            }

        }

        private ICommand _OnSelectCustomImage;

        public ICommand OnSelectCustomImage
        {
            get
            {
                if (_OnSelectCustomImage == null)
                {
                    _OnSelectCustomImage = new RelayCommand(obj =>
                    {
                        var dialog = new OpenFileDialog() { Filter = "Image files (*.BMP, *.JPG, *.GIF, *.TIF, *.PNG, *.ICO, *.EMF, *.WMF)|*.bmp;*.jpg;*.gif; *.tif; *.png; *.ico; *.emf; *.wmf" };

                        if (dialog.ShowDialog() == true)
                        {
                            CustomImage = dialog.FileName;
                        }
                    });
                }
                return _OnSelectCustomImage;
            }

        }




        public void UpdateProperty()
        {
            OnPropertyChanged("SelectedOrder");
            OnPropertyChanged("DiscountText");
            OnPropertyChanged("SumWithDiscount");
            OnPropertyChanged("Price");
            OnPropertyChanged("VanileImage");
            OnPropertyChanged("CustomImage");
            OnPropertyChanged("isOrderSelected");
        }
    }
}
