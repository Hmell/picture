﻿using BusinessPicture.Helpers;
using GalaSoft.MvvmLight.CommandWpf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace BusinessPicture.ViewModels
{
    class BasePageViewModel : ViewModelBase
    {

        private readonly DBManager db = new DBManager();

        public ObservableCollection<PictureSize> PicSize  { get; set; }
        public ObservableCollection<PictureStyle> PicStyle { get; set; }
        public ObservableCollection<Additional> Additionals { get; set; }
        public ObservableCollection<User> Users { get; set; }

        public String Nlogin { get; set; }
        public String Npassword { get; set; }
        public String Nwidth { get; set; }
        public String Nheight { get; set; }
        public String NstyleName { get; set; }
        public String NstyleDesc { get; set; }


        public BasePageViewModel()
        {
            _isAdmin = AppContext.user.ToLower().Equals("admin");
            PicSize = new ObservableCollection<PictureSize>(db.GetEntityes(DBManager.Entity.E_PICSIZE).Select(b => (PictureSize) b ));
            PicStyle = new ObservableCollection<PictureStyle>(db.GetEntityes(DBManager.Entity.E_PICSTYLE).Select(b => (PictureStyle) b ));
            Additionals = new ObservableCollection<Additional>(db.GetEntityes(DBManager.Entity.E_Ad).Select(b => (Additional) b ));
            Users = new ObservableCollection<User>(db.GetEntityes(DBManager.Entity.E_USER).Select(b => (User) b ));

        }

        private bool _isAdmin;

        public Visibility isAdmin
        {
            get { return !_isAdmin ? Visibility.Visible : Visibility.Collapsed; }
        }

        private PictureSize selectedPicSize;

        public PictureSize SelectedPicSize
        {
            get { return selectedPicSize; }
            set
            {
                if (value != null)
                {
                    selectedPicSize = value;
                    OnPropertyChanged("SelectedPicSize");
                }
            }
        }

        private PictureStyle selectedPicStyle;

        public PictureStyle SelectedPicStyle
        {
            get { return selectedPicStyle; }
            set
            {
                if (value != null)
                {
                    selectedPicStyle = value;
                    OnPropertyChanged("SelectedPicStyle");
                }
            }
        }

        private User selectedUser;

        public User SelectedUser
        {
            get { return selectedUser; }
            set
            {
                if (value != null)
                {
                    selectedUser = value;
                    OnPropertyChanged("SelectedUser");
                }
            }
        }

        private Additional selectedAddit;

        public Additional SelectedAddit
        {
            get { return selectedAddit; }
            set
            {
                if (value != null)
                {
                    selectedAddit = value;
                    OnPropertyChanged("SelectedAddit");
                }
            }
        }



        private ICommand _onAddUser;

        public ICommand OnAddUser
        {
            get
            {
                if (_onAddUser == null)
                {
                    _onAddUser = new RelayCommand(obj =>
                    {

                        int id = DBManager.AddEntity(DBManager.Entity.E_USER, new User(-1, Nlogin, Npassword));
                        if (id != -1)
                            Users.Add(new User(id, Nlogin, Npassword));
                    });
                }
                return _onAddUser;
            }
        }

        private ICommand _onAddSize;

        public ICommand OnAddSize
        {
            get
            {
                if (_onAddSize == null)
                {
                    _onAddSize = new RelayCommand(obj =>
                    {

                        int id = DBManager.AddEntity(DBManager.Entity.E_PICSIZE, new PictureSize(-1, Double.Parse(Nwidth), Double.Parse(Nheight)));
                        if (id != -1)
                            PicStyle.Add(new PictureStyle(id, NstyleName, NstyleDesc));
                    });
                }
                return _onAddSize;
            }
        }


        private ICommand _onAddStyle;

        public ICommand OnAddStyle
        {
            get
            {
                if (_onAddStyle == null)
                {
                    _onAddStyle = new RelayCommand(obj =>
                    {

                        int id = DBManager.AddEntity(DBManager.Entity.E_PICSTYLE, new PictureStyle(-1, NstyleName, NstyleDesc));
                        if (id != -1)
                           PicStyle.Add(new PictureStyle(id, NstyleName, NstyleDesc));
                    });
                }
                return _onAddStyle;
            }
        }


        private ICommand _onSaveChange;

        public ICommand OnSaveChange
        {
            get
            {
                if (_onSaveChange == null)
                {
                    _onSaveChange = new RelayCommand(obj =>
                    {
                        if ((obj as DataGridRow).DataContext.GetType() == typeof(PictureSize))
                        {
                            PictureSize ps = (obj as DataGridRow).DataContext as PictureSize;
                            DBManager.UpdateEntity(DBManager.Entity.E_PICSIZE, ps);
                        }
                        else if ((obj as DataGridRow).DataContext.GetType() == typeof(PictureStyle))
                        {
                            PictureStyle ps = (obj as DataGridRow).DataContext as PictureStyle;
                            DBManager.UpdateEntity(DBManager.Entity.E_PICSTYLE, ps);
                        }
                        else if ((obj as DataGridRow).DataContext.GetType() == typeof(User))
                        {
                            User ps = (obj as DataGridRow).DataContext as User;
                            DBManager.UpdateEntity(DBManager.Entity.E_USER, ps);
                        }
                        else if ((obj as DataGridRow).DataContext.GetType() == typeof(Additional))
                        {
                            Additional ps = (obj as DataGridRow).DataContext as Additional;
                            DBManager.UpdateEntity(DBManager.Entity.E_Ad, ps);
                        }

                    });
                }
                return _onSaveChange;
            }
        }

        private ICommand _onDelete;

        public ICommand OnDelete
        {
            get
            {
                if (_onDelete == null)
                {
                    _onDelete = new RelayCommand(obj =>
                    {
                        if((obj as DataGridRow).DataContext.GetType() == typeof(PictureSize))
                        {
                            PictureSize ps = (obj as DataGridRow).DataContext as PictureSize;
                            if(DBManager.DeleteEntity(DBManager.Entity.E_PICSIZE, ps.Id))
                                PicSize.Remove(ps);
                        }else if ((obj as DataGridRow).DataContext.GetType() == typeof(PictureStyle))
                        {
                            PictureStyle ps = (obj as DataGridRow).DataContext as PictureStyle;
                            if(DBManager.DeleteEntity(DBManager.Entity.E_PICSTYLE, ps.Id))
                            PicStyle.Remove(ps);
                        }else if((obj as DataGridRow).DataContext.GetType() == typeof(User))
                        {
                            User ps = (obj as DataGridRow).DataContext as User;
                            if(DBManager.DeleteEntity(DBManager.Entity.E_USER, ps.Id))
                                Users.Remove(ps);
                        }

                    });
                }
                return _onDelete;
            }
        }


        public class User
        {
            public User(int id, string login, string password)
            {
                this.Id = id;
                this.Login = login;
                this.Password = password;
            }

            public User(object[] obj)
            {
                this.Id = obj[0] != null ? (int)obj[0] : 0;
                this.Login = obj[1] != null ? (string)obj[1] : "";
                this.Password = obj[2] != null ? (string)obj[2] : "";
            }

            public int Id { get; set; }
            public string Login { get; set; }
            public string Password { get; set; }
        }

    }

    public class RelayCommand : ICommand
    {
        private Action<object> execute;
        private Func<object, bool> canExecute;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public RelayCommand(Action<object> execute, Func<object, bool> canExecute = null)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return this.canExecute == null || this.canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            this.execute(parameter);
        }
    }
}
