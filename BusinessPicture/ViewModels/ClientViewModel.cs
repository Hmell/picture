﻿using BusinessPicture.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessPicture.ViewModels
{
    class ClientViewModel : ViewModelBase
    {
        public Client client;

        public ClientViewModel(Client client)
        {
            this.client = client;
        }

        public int id
        {
            get { return client.Id; }
            set
            {
                client.Id = value;
                OnPropertyChanged("id");
            }
        }

        public string First_name
        {
            get { return client.First_name; }
            set
            {
                client.First_name = value;
                OnPropertyChanged("First_name");
            }
        }

        public string Last_name
        {
            get { return client.Last_name; }
            set
            {
                client.Last_name = value;
                OnPropertyChanged("Last_name");
            }
        }

        public string Father_name
        {
            get { return client.Father_name; }
            set
            {
                client.Father_name = value;
                OnPropertyChanged("Father_name");
            }
        }

        public string Phone
        {
            get { return client.Phone; }
            set
            {
                client.Phone = value;
                OnPropertyChanged("Phone");
            }
        }

        public string Email
        {
            get { return client.Email; }
            set
            {
                client.Email = value;
                OnPropertyChanged("Email");
            }
        }

        public string Description
        {
            get { return client.Description; }
            set
            {
                client.Description = value;
                OnPropertyChanged("Description");
            }
        }



    }
}
