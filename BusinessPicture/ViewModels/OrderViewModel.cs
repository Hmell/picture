﻿using BusinessPicture.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessPicture.ViewModels
{
    class OrderViewModel : ViewModelBase
    {

        public Order order;

        public OrderViewModel(Order order)
        {
            this.order = order;
        }

        public int Id
        {
            get { return order.Id; }
            set
            {
                order.Id = value;
                OnPropertyChanged("id");
            }
        }

        public Client Client
        {
            get { return order.Client; }
            set
            {
                order.Client = value;
                OnPropertyChanged("Client");
            }
        }

        public string ClientShortName
        {
            get 
            {
                if (Client != null)
                    return string.Format("{0} {1}. {2}.", Client.Last_name, Client.First_name.Substring(0, 1), Client.Father_name.Substring(0, 1));
                else return "";
            }
        }

        public Picture Picture
        {
            get { return order.Picture; }
            set
            {
                order.Picture = value;
                OnPropertyChanged("Picture");
            }
        }

        public int quantity
        {
            get { return order.Quantity; }
            set
            {
                order.Quantity = value;
                OnPropertyChanged("Quantity");
            }
        }

        public int Discount
        {
            get { return order.Discount; }
            set
            {
                order.Discount = value;
                OnPropertyChanged("Discount");
            }
        }

        public string DiscountText
        {
            get { return order.Discount+"%"; }
            set
            {
                string distext = value;
                order.Discount = Int32.Parse(distext.Replace('%',' ').Trim());
                OnPropertyChanged("DiscountText");
            }
        }

        public Decimal Price
        {
            get { return order.Price; }
            set
            {
               order.Price = value;
                OnPropertyChanged("Price");
            }
        }

        public Decimal TotalPrice 
        { 
                      
            get {
                decimal total = 1490M;
                total *= quantity;
                if (Picture != null)
                {
                    double wh = Picture.Size.Width + Picture.Size.Height - 60;
                    if (wh >= 10)
                    {
                        total += ((decimal)Math.Ceiling(wh / 10)) * 200M;
                    }
                    if (Picture.Style.Id > 1)
                    {
                        total += 1100M;
                        total += Picture.PeopleOnPicture > 2 ? (Picture.PeopleOnPicture - 2) * 200M : 0M;
                    }
                }

                total += NeedDelivery ? AppContext.PriceDelivery : 0;
                total += NeedPackage ? AppContext.PricePackage : 0;

                return total; 
            }
        }

        public string Notation
        {
            get { return order.Notation; }
            set
            {
                order.Notation = value;
                OnPropertyChanged("Notation");
            }
        }

        public string Done
        {
            get { return order.Done ? "Выполнено" : "В процессе"; }
            set
            {
                order.Done = false;
                OnPropertyChanged("Done");
            }
        }

        public bool IsDone
        {
            get { return order.Done; }
            set { order.Done = value; }
        }

        public DateTime Date
        {
            get { return order.Date; }
            set { order.Date = value; }
        }

        public bool NeedPackage
        {
            get { return order.NeedPackage; }
            set
            {
                order.NeedPackage = value;
                OnPropertyChanged("Done");
            }
        }

        public bool NeedDelivery
        {
            get { return order.NeedDelivery; }
            set
            {
                order.NeedDelivery = value;
                OnPropertyChanged("Done");
            }
        }



    }
}
