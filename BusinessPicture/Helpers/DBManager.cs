﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using static BusinessPicture.ViewModels.BasePageViewModel;

namespace BusinessPicture.Helpers
{
    class DBManager
    {

        public static MySqlConnection GetDBConnection()
        {

            string host = Properties.Resources.host;
            int port = Int32.Parse(Properties.Resources.port);
            string database = Properties.Resources.database;
            string username = Properties.Resources.user;
            string password = "root" ; 

            String connString = "Server=" + host + ";Database=" + database
                + ";port=" + port + ";User Id=" + username + ";password=" + password;

            MySqlConnection conn = new MySqlConnection(connString);

            return conn;
        }


        public ObservableCollection<object> GetEntityes(Entity entity)
        {
            ObservableCollection<object> entityList = new ObservableCollection<object>(); 
            
            MySqlConnection conn = GetDBConnection();

            try
            {
                conn.Open();
                string sql = Properties.Resources.ResourceManager.GetString(entity.ToString());

            MySqlCommand cmd = new MySqlCommand(sql, conn);

            using (DbDataReader reader = cmd.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                            object[] list = new object[reader.FieldCount];
                            reader.GetValues(list);

                            switch (entity)
                            {
                                case Entity.E_CLIENT:
                                    entityList.Add(new Client(list)); break;
                                case Entity.E_ORDER:
                                    {
                                        bool[] add = getAdditional((int)list[0]);
                                        entityList.Add(new Order(list, add[0], add[1])); break;
                                    }
                                case Entity.E_PICSIZE:
                                    entityList.Add(new PictureSize(list)); break;
                                case Entity.E_PICSTYLE:
                                    entityList.Add(new PictureStyle(list)); break;
                                case Entity.E_Ad:
                                    entityList.Add(new Additional(list)); break;
                                case Entity.E_USER:
                                    entityList.Add(new User(list)); break;
                            }
                    }
                }
            }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e);
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return entityList;
        }

        public ObservableCollection<object> GetOrderForClients(Entity entity, int id)
        {
            ObservableCollection<object> entityList = new ObservableCollection<object>();

            MySqlConnection conn = GetDBConnection();
            conn.Open();
            try
            {
                string sql = Properties.Resources.ResourceManager.GetString(entity.ToString()) + " Where Client_idClient =" + id;

                MySqlCommand cmd = new MySqlCommand(sql, conn);

                using (DbDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            object[] list = new object[reader.FieldCount];
                            reader.GetValues(list);

                            bool[] add = getAdditional((int)list[0]);
                            entityList.Add(new Order(list, add[0], add[1]));

                            
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e);
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return entityList;
        }


        public object GetEntity(Entity entityT, int id)
        {
            object entity = null;

            MySqlConnection conn = GetDBConnection();
            conn.Open();
            try
            {

                string sql = Properties.Resources.ResourceManager.GetString(entityT.ToString()) 
                    + " Where " + (entityT == Entity.E_PICTURE ? "p." : "") + "id = " + id + ";";


                MySqlCommand cmd = new MySqlCommand(sql, conn);

                using (DbDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            object[] list = new object[reader.FieldCount];
                            reader.GetValues(list);

                            switch (entityT)
                            {
                                case Entity.E_CLIENT:
                                    entity = new Client(list); break;
                                case Entity.E_ORDER:
                                    {
                                        bool[] add = getAdditional((int)list[0]);
                                        entity = new Order(list, add[0], add[1]); break;
                                    }
                                case Entity.E_PICTURE:
                                    entity = new Picture(list); break;
                                case Entity.E_PICSIZE:
                                    entity = new PictureSize(list); break;
                                case Entity.E_PICSTYLE:
                                    entity = new PictureStyle(list); break;
                                case Entity.E_Ad:
                                    entity = new Additional(list); break;
                                case Entity.E_USER:
                                    entity = new User(list); break;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e);
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return entity;
        }


        public static int AddEntity(Entity en, object entity)
        {
            int reId = -1;

            MySqlConnection connection = GetDBConnection();
            connection.Open();
            try
            {
                string sql = Properties.Resources.ResourceManager.GetString(en.ToString()+"_ADD");

                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sql;

                switch (en)
                {
                    case Entity.E_CLIENT:
                        Client client = (Client)entity;
                        cmd.Parameters.Add("@First_Name", MySqlDbType.VarChar).Value = client.First_name;
                        cmd.Parameters.Add("@Last_Name", MySqlDbType.VarChar).Value = client.Last_name;
                        cmd.Parameters.Add("@Father_Name", MySqlDbType.VarChar).Value = client.Father_name;
                        cmd.Parameters.Add("@Phone", MySqlDbType.VarChar).Value = client.Phone;
                        cmd.Parameters.Add("@eMail", MySqlDbType.VarChar).Value = client.Email;
                        cmd.Parameters.Add("@Description", MySqlDbType.VarChar).Value = client.Description;
                        break;
                    case Entity.E_ORDER:
                        Order order = (Order)entity;
                        if(order.Client != null) 
                            cmd.Parameters.Add("@Client_idClient", MySqlDbType.Int32).Value = order.Client.Id;
                        cmd.Parameters.Add("@Picture_idPicture", MySqlDbType.Int32).Value = order.Picture.Id;
                        cmd.Parameters.Add("@Price", MySqlDbType.Decimal).Value = order.Price;
                        cmd.Parameters.Add("@Notation", MySqlDbType.VarChar).Value = order.Notation;
                        cmd.Parameters.Add("@Done", MySqlDbType.Binary).Value = order.Done ? 1 : 0;
                        cmd.Parameters.Add("@Quantity", MySqlDbType.Int32).Value = order.Quantity;
                        cmd.Parameters.Add("@Discount", MySqlDbType.Int32).Value = order.Discount;
                        cmd.Parameters.Add("@Date", MySqlDbType.LongBlob).Value = order.Date.Ticks;
                        
                        break;
                    case Entity.E_PICTURE:
                        Picture picture = (Picture)entity;
                        cmd.Parameters.Add("@PicStyle_idPicStyle", MySqlDbType.Int32).Value = picture.Style.Id;
                        cmd.Parameters.Add("@PicSize_idPicSize", MySqlDbType.Int32).Value = picture.Size.Id;
                        cmd.Parameters.Add("@PeopleOnPicture", MySqlDbType.Int32).Value = picture.PeopleOnPicture;
                        break;
                    case Entity.E_USER:
                        User user = (User)entity;
                        cmd.Parameters.Add("@login", MySqlDbType.VarChar).Value = user.Login;
                        cmd.Parameters.Add("@password", MySqlDbType.VarChar).Value = HashPassword(user.Password);
                        break;
                }
                cmd.ExecuteNonQuery();
                reId = (int)cmd.LastInsertedId;
                // Int32 userCount = (Int32)cmd.ExecuteScalar();



            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e);
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }

            return reId;
        }


        public static bool UpdateEntity(Entity en, object entity)
        {
            bool req = false;

            MySqlConnection connection = GetDBConnection();
            connection.Open();
            try
            {
                string sql = Properties.Resources.ResourceManager.GetString(en.ToString() + "_UP");

                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sql;

                switch (en)
                {
                    case Entity.E_CLIENT:
                        Client client = (Client)entity;
                        cmd.Parameters.Add("@id", MySqlDbType.Int32).Value = client.Id;
                        cmd.Parameters.Add("@First_Name", MySqlDbType.VarChar).Value = client.First_name;
                        cmd.Parameters.Add("@Last_Name", MySqlDbType.VarChar).Value = client.Last_name;
                        cmd.Parameters.Add("@Father_Name", MySqlDbType.VarChar).Value = client.Father_name;
                        cmd.Parameters.Add("@Phone", MySqlDbType.VarChar).Value = client.Phone;
                        cmd.Parameters.Add("@eMail", MySqlDbType.VarChar).Value = client.Email;
                        cmd.Parameters.Add("@Description", MySqlDbType.VarChar).Value = client.Description;
                        break;
                    case Entity.E_ORDER:
                        Order order = (Order)entity;
                        cmd.Parameters.Add("@id", MySqlDbType.Int32).Value = order.Id;
                        cmd.Parameters.Add("@Client_idClient", MySqlDbType.Int32).Value = order.Client.Id;
                        cmd.Parameters.Add("@Price", MySqlDbType.Decimal).Value = order.Price;
                        cmd.Parameters.Add("@Notation", MySqlDbType.VarChar).Value = order.Notation;
                        cmd.Parameters.Add("@Done", MySqlDbType.Binary).Value = order.Done ? 1 : 0;
                        cmd.Parameters.Add("@Quantity", MySqlDbType.Int32).Value = order.Quantity;
                        cmd.Parameters.Add("@Discount", MySqlDbType.Int32).Value = order.Discount;
                        cmd.Parameters.Add("@Date", MySqlDbType.LongBlob).Value = order.Date.Ticks;
                        break;
                    case Entity.E_PICTURE:
                        Picture picture = (Picture)entity;
                        cmd.Parameters.Add("@id", MySqlDbType.Int32).Value = picture.Id;
                        cmd.Parameters.Add("@PicStyle_idPicStyle", MySqlDbType.Int32).Value = picture.Style.Id;
                        cmd.Parameters.Add("@PicSize_idPicSize", MySqlDbType.Int32).Value = picture.Size.Id;
                        cmd.Parameters.Add("@PeopleOnPicture", MySqlDbType.Int32).Value = picture.PeopleOnPicture;
                        cmd.Parameters.Add("@VanileImage", MySqlDbType.VarChar).Value = picture.VanileImage;
                        cmd.Parameters.Add("@CustomImage", MySqlDbType.VarChar).Value = picture.CustomImage;
                        break;
                    case Entity.E_USER:
                        User user = (User)entity;
                        cmd.Parameters.Add("@id", MySqlDbType.Int32).Value = user.Id;
                        cmd.Parameters.Add("@login", MySqlDbType.VarChar).Value = user.Login;
                        cmd.Parameters.Add("@password", MySqlDbType.VarChar).Value = HashPassword(user.Password);

                        break;
                    case Entity.E_PICSIZE:
                        PictureSize pictureSize = (PictureSize)entity;
                        cmd.Parameters.Add("@id", MySqlDbType.Int32).Value = pictureSize.Id;
                        cmd.Parameters.Add("@width", MySqlDbType.Double).Value = pictureSize.Width;
                        cmd.Parameters.Add("@height", MySqlDbType.Double).Value = pictureSize.Height;
                        break;
                    case Entity.E_PICSTYLE:
                        PictureStyle pictureStyle = (PictureStyle)entity;
                        cmd.Parameters.Add("@id", MySqlDbType.Int32).Value = pictureStyle.Id;
                        cmd.Parameters.Add("@name", MySqlDbType.VarChar).Value = pictureStyle.Name;
                        cmd.Parameters.Add("@description", MySqlDbType.VarChar).Value = pictureStyle.Description;
                        break;
                    case Entity.E_Ad:
                        Additional additional = (Additional)entity;
                        cmd.Parameters.Add("@id", MySqlDbType.Int32).Value = additional.Id;
                        cmd.Parameters.Add("@caption", MySqlDbType.VarChar).Value = additional.Caption;
                        cmd.Parameters.Add("@price", MySqlDbType.Double).Value = additional.Price;
                        break;
                }

                cmd.ExecuteNonQuery();

                req = true;

            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e);
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }

            return req;
        }


        public static bool DeleteEntity(Entity en, int id)
        {
            bool req = false;

            MySqlConnection connection = GetDBConnection();
            connection.Open();
            try
            {
                string sql = Properties.Resources.ResourceManager.GetString(en.ToString() + "_DEL");

                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = sql;

                cmd.Parameters.Add("@id", MySqlDbType.Int32).Value = id;
/*                switch (en)
                {
                    case Entity.E_CLIENT:
                        cmd.Parameters.Add("@id", MySqlDbType.Int32).Value = id;
                        break;
                    case Entity.E_ORDER:
                        cmd.Parameters.Add("@id", MySqlDbType.Int32).Value = id;
                        break;
                        break;
                    case Entity.E_PICTURE:

                        break;
                }*/

                cmd.ExecuteNonQuery();

                req = true;

            }
            catch (Exception e)
            {
                MessageBox.Show("Не возможно удалить, возможно элемент имеет внешние связи",
                                          "Уведомление",
                                          MessageBoxButton.OK,
                                          MessageBoxImage.Information);
                Console.WriteLine("Error: " + e);
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }

            return req;
        }

        private bool[] getAdditional(int id)
        {
            bool[] add = new bool[2];

            MySqlConnection conn = GetDBConnection();
            conn.Open();
            try
            {
                string sql = "SELECT * FROM additional_has_order where Order_idOrder = " + id;

                MySqlCommand cmd = new MySqlCommand(sql, conn);

                using (DbDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            object[] list = new object[reader.FieldCount];
                            reader.GetValues(list);

                            if (list.Length == 2)
                            {
                                add[0] = ((int)list[0] == 1) || ((int)list[1] == 1);
                                add[1] = ((int)list[0] == 2) || ((int)list[1] == 2);
                            }else if(list.Length == 1)
                            {
                                add[0] = (int)list[0] == 1;
                                add[1] = (int)list[0] == 2;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e);
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }

            return add;
        }

        public bool LogIn(string login,string password)
        {

            string sql = "";
            string hashPass = null;
            MySqlConnection conn = GetDBConnection();
            conn.Open();
            
                sql = $"SELECT password FROM `user` WHERE `login` = @login;";

            using (var cmd = new MySqlCommand(sql, conn))
            {
                cmd.Parameters.Add(new MySqlParameter("@login", login));
                using (DbDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            hashPass = reader.GetString(0);
                        }
                    }
                }
            }

            if(hashPass != null)
            {
               return VerifyHashedPassword(hashPass, password);
            }
            return false;
        }

        public static string HashPassword(string password)
        {
            byte[] salt;
            byte[] buffer2;
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }
            using (Rfc2898DeriveBytes bytes = new Rfc2898DeriveBytes(password, 0x10, 0x3e8))
            {
                salt = bytes.Salt;
                buffer2 = bytes.GetBytes(0x20);
            }
            byte[] dst = new byte[0x31];
            Buffer.BlockCopy(salt, 0, dst, 1, 0x10);
            Buffer.BlockCopy(buffer2, 0, dst, 0x11, 0x20);
            return Convert.ToBase64String(dst);
        }

        public static bool VerifyHashedPassword(string hashedPassword, string password)
        {
            byte[] buffer4;
            if (hashedPassword == null)
            {
                return false;
            }
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }
            byte[] src = Convert.FromBase64String(hashedPassword);
            if ((src.Length != 0x31) || (src[0] != 0))
            {
                return false;
            }
            byte[] dst = new byte[0x10];
            Buffer.BlockCopy(src, 1, dst, 0, 0x10);
            byte[] buffer3 = new byte[0x20];
            Buffer.BlockCopy(src, 0x11, buffer3, 0, 0x20);
            using (Rfc2898DeriveBytes bytes = new Rfc2898DeriveBytes(password, dst, 0x3e8))
            {
                buffer4 = bytes.GetBytes(0x20);
            }
            return ByteArraysEqual(buffer3, buffer4);
        }

        public static bool ByteArraysEqual(byte[] b1, byte[] b2)
        {
            if (b1 == b2) return true;
            if (b1 == null || b2 == null) return false;
            if (b1.Length != b2.Length) return false;
            for (int i = 0; i < b1.Length; i++)
            {
                if (b1[i] != b2[i]) return false;
            }
            return true;
        }

        public enum Entity
        {
             E_CLIENT,
             E_ORDER,
             E_PICTURE,
             E_PICSIZE,
             E_PICSTYLE,
             E_Ad,
             E_USER
        }
    }
}
