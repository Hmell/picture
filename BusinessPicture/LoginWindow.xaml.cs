﻿using BusinessPicture.Helpers;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BusinessPicture
{
    /// <summary>
    /// Логика взаимодействия для LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void OnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OnLogIn_Click(object sender, RoutedEventArgs e)
        {
            er.Visibility = Visibility.Collapsed;
            DBManager db = new DBManager();
            bool verify = db.LogIn(login.Text, password.Password);

            if (verify)
            {
                AppContext appc = new AppContext();
                AppContext.user = login.Text;
                new MainWindow().Show();
                this.Close();
            }
            else er.Visibility = Visibility.Visible;
        }

        private void RowDefinition_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
    }
}
