﻿using BusinessPicture.Helpers;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace BusinessPicture
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private Rect _restoreLocation;

        public MainWindow()
        {
            InitializeComponent();
            user.Text = AppContext.user;
            AppContext.fr = frame;
            DBManager db = new DBManager();
            ObservableCollection<Additional> al = new ObservableCollection<Additional>(db.GetEntityes(DBManager.Entity.E_Ad).Select(b => (Additional)b));
            AppContext.PriceDelivery = (decimal)al[0].Price;
            AppContext.PricePackage = (decimal)al[0].Price;
            MaxHeight = SystemParameters.MaximizedPrimaryScreenHeight;

            //frame.Content = new mainPage();
        }

        private void BtnList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBoxItem lb =  (ListBoxItem)btnList.SelectedItem;
            string uri = "Pages/" + (string)lb.Tag;
            frame.NavigationService.Navigate( new Uri(uri, UriKind.Relative));

        }


        public void AddOrder()
        {
            string uri = "Pages/" + 1;
            frame.NavigationService.Navigate(new Uri(uri, UriKind.Relative));
        }

        private void OnClose(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void OnMinimaized(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }
        private void OnMaximaized(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
        }

        private void RowDefinition_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        
        private void OnChangeUser(object sender, RoutedEventArgs e)
        {
 
            new LoginWindow().Show();
            Close();
        }
    }



    enum PageName
    {
        PAGE_MAIN = 0,
        PAGE_ORDER = 1,
        PAGE_CLIENT = 2,
        PAGE_BD = 3


    }
}
